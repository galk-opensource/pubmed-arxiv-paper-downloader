package rivendell;

import downloader.download_util.HttpHandler;
import utils.Logger.LoggerConsole;
import utils.Pair;

import java.util.ArrayList;
import java.util.List;

/*
This class manage the work with Rivendell's API and allow to send query and get all PDF's
or getting a list of links for PDFs from Pubmed and Arxiv
 */
public class RivendellApi
{
    // Logger //
    public static LoggerConsole logger = new LoggerConsole();
    // end - Logger //

    // consts //
    private static final String RINDELL_API_URL = "http://rivendell.cs.biu.ac.il/Api/";

    // rivendell api functions elements
    private static final String GET_EXPERIMENT_RECORD = "getExperimentRecord";
    private static final String GET_ALL_NANO = "getAllNano";
    private static final String GET_ALL_NANO_JSON = "getAllNanoJson";
    private static final String RUN_QUERY = "RunQuery";
    private static final String RUN_QUERY_JSON = "runQueryJson";
    private static final String GET_BLACK_LIST_JSON = "blackList";
    private static final String CHECK_PREMISION_LEVEL = "CheckPremisionLevel";

    // db codes
    public static final int DB_ALL = 1;
    public static final int DB_PUBMED = 2;
    public static final int DB_ARXIV = 3;
    public static final int SEMANTIC = 4;

    // end - consts //

    // members //
    private String key;
    // end - members //

    // full constractor
    public RivendellApi(String newKey)
    {
        this.key = newKey;
    }

    // get functions //

    public String getKey()
    {
        logger.addLineToLog("return key for rivendell API");
        return this.key;
    }

    public String getRindellApiUrl()
    {
        return RINDELL_API_URL;
    }

    // end - get functions //

    /**
     * this functions call the Rivendell API for getting the abstracts we want
     * @param query - the query to Rivendell to run
     * @param db - the db we want to search on
     * @param isAll - return all or not
     * @param count - if not return all, return this random amount
     * @return - list of abstracts relevent to us
     */
    public List<AbstractRivendell> runQuery(String query, Integer db, Boolean isAll, Integer count)
    {
        logger.addLineToLog("start runQuery");

        // build the api needed parameters
        List<Pair<String,String>> parameters = new ArrayList<>();
        parameters.add(new Pair<String, String>("key",key));
        parameters.add(new Pair<String, String>("query",query));
        parameters.add(new Pair<String, String>("dB",db.toString()));
        parameters.add(new Pair<String, String>("isAll",isAll.toString()));
        parameters.add(new Pair<String, String>("HowMany",count.toString()));

        // send request
        String rivendellAnswer;
        try
        {
            rivendellAnswer = HttpHandler.sendGet(getRindellApiUrl() + RUN_QUERY, parameters);
            logger.addLineToLog("Got RUN_QUERY answer");
        }
        catch (Exception ex)
        {
           logger.addLineToLog("Get request in runQuery in RivendellApi with error: " + ex.getMessage());
           return null;
        }
        return AbstractRivendell.toObject(rivendellAnswer);
    }

    /**
     * Defulat run query property function - overloading the properties of the search
     * @param query - the query we want Rivendell to run
     * @return the abstracts in easy to return format
     */
    public List<AbstractRivendell> runQuery(String query)
    {
        return runQuery(query, DB_ALL, true, 1000);
    }

    /**
     * get the data from Rivendell about the nano proj backdoor results
     * @return the abstracts we find relevent
     */
    public List<AbstractRivendell> getAllNano()
    {
        logger.addLineToLog("start getAllNano");

        List<Pair<String,String>> parameters = new ArrayList<>();
        parameters.add(new Pair<String, String>("key",key));
        // send request
        String rivendellAnswer;
        try
        {
            rivendellAnswer = HttpHandler.sendGet(getRindellApiUrl() + GET_ALL_NANO, parameters);
            logger.addLineToLog("Got getAllNano answer");
        }
        catch (Exception ex)
        {
            new LoggerConsole("Get request in getAllNano in RivendellApi with error: " + ex.getMessage());
            return null;
        }

        String[] taged = rivendellAnswer.split(AbstractRivendell.NO_ABSTRACTS_SPLITER);
        final int NANO_AND_NOT_SIZE = 2;
        if(taged.length == NANO_AND_NOT_SIZE)
        {
            List<AbstractRivendell> answer = AbstractRivendell.toObject(taged[0], true);
            answer.addAll(AbstractRivendell.toObject(taged[1], false));
            return answer;
        }
        else // should not happen - if we have 1 query which is not nano related
        {
            logger.addLineToLog("we got wrong answer format from Rivendell about the all nano function");
            return null;
        }
    }

    // toString //

    public String toString()
    {
        return "{\"key\":\"" + this.key + "\", \"api_link\":\"" + RivendellApi.RINDELL_API_URL + "\"}";
    }

    // end - toString //
}
