package rivendell;

import utils.Logger.LoggerConsole;
import utils.Pair;

import java.util.ArrayList;
import java.util.List;

public class ArticleRivendell
{
    // Logger //
    public static LoggerConsole logger = new LoggerConsole();
    // end - Logger //

    // coints //
    public static int UNKNOWN = AbstractRivendell.UNKNOWN;
    public static int RELEVENT = AbstractRivendell.RELEVENT;
    public static int NOT_RELEVENT = AbstractRivendell.NOT_RELEVENT;
    public static int ALL = AbstractRivendell.ALL;
    // end - consts //

    // members //
    private int isRelevent;
    private String pdfLocation;
    private String pmid;
    // end - members //

    // empty constractor
    public ArticleRivendell(String newPdfLocation, String Pmid)
    {
        fulfil(newPdfLocation, pmid);
        this.isRelevent = AbstractRivendell.UNKNOWN;
    }

    // full constractor
    public ArticleRivendell(String newPdfLocation, int isRelevent, String pmid)
    {
        fulfil(newPdfLocation, pmid);
        this.isRelevent = isRelevent;
    }

    private void fulfil(String newPdfLocation, String pmid)
    {
        this.pdfLocation = newPdfLocation;
        this.pmid = pmid;
    }

    public String getPmid()
    {
        return this.pmid;
    }

    public String getArticle()
    {
        return this.pdfLocation;
    }

    public void setRelevent(int relevent)
    {
        if (relevent == RELEVENT || relevent == NOT_RELEVENT || relevent == UNKNOWN)
        {
            this.isRelevent = relevent;
        }
        logger.addLineToLog("cannot set " + relevent + " value");
    }

    public static List<Pair<String, String>> exctractLinks(List<Pair<ArticleRivendell, String>> articleRivendellList)
    {
        List<Pair<String, String>> answer = new ArrayList<>();
        for (Pair<ArticleRivendell, String> aritleTitle: articleRivendellList)
        {
            answer.add(new Pair<String, String>(aritleTitle.getFirst().pdfLocation, aritleTitle.getSecond()));
        }
        return answer;
    }

    @Override
    public String toString()
    {
        return "{isRelevent=" + isRelevent + ", pdfLocation='" + pdfLocation + '\'' + ", pmid='" + pmid + '\'' + '}';
    }
}