package rivendell.rivendell_nano_filter;

import org.jetbrains.annotations.NotNull;
import utils.Pair;

/*
    Just a pair but with meaningful operations
 */
public class WordSignal extends Pair<String, Integer> implements Comparable<WordSignal>
{
    public WordSignal(String word, Integer count)
    {
        this.setFirst(word);
        this.setSecond(count);
    }

    ///////////
    // logic //
    ///////////

    public static Boolean before(WordSignal a, WordSignal b)
    {
        return a.getCount() >= b.getCount();
    }

    /////////////////
    // end - logic //
    /////////////////

    /////////////
    // actions //
    /////////////

    public void increase()
    {
        this.increase(1);
    }

    public void increase(int howMuch)
    {
        this.setSecond(this.getSecond() + howMuch);
    }

    ////////////////////
    // end - actions  //
    ////////////////////

    /////////////
    // getters //
    /////////////

    public String getWord()
    {
        return this.getFirst();
    }

    public Integer getCount()
    {
        return this.getSecond();
    }

    @Override
    public int compareTo(@NotNull WordSignal ws)
    {
        return this.getCount().compareTo(ws.getCount());
    }

    ///////////////////
    // end - getters //
    ///////////////////
}
