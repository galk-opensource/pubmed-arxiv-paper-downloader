package rivendell.rivendell_nano_filter;

import rivendell.AbstractRivendell;
import utils.Logger.LoggerConsole;
import utils.Math_Nlp.TextToVec;
import utils.Pair;
import utils.ml.logistic_reqression.LogisticRegression;
import utils.ml.naive_bayes.BayesClassifier;
import utils.ml.naive_bayes.Classifier;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/*
    gets a list of abstracts, a set of queries and relevent results from Rivendell
    and return the results we have that we think is close enough

    1. We use Logistic regression as a binary classification for raw data
 */
public class NanoArticleClassifiar
{
    // Logger //
    public static LoggerConsole logger = new LoggerConsole();
    // end - Logger //

    // default consts
    public static final Double EPSILON = 0.001;
    public static final Integer FEATURE_SIZE = 20;
    // end - consts //

    // members //
    // learning meta-data
    private Double epsilon;
    private Integer featureSize;

    // model and uses
    private LogisticRegression model;
    private List<String> leadingSignals;
    // end - members //

    // full constractor
    public NanoArticleClassifiar(List<String> nanoTagedAbstracts,
                                 List<AbstractRivendell> markedAbstracts,
                                 Double epsilon,
                                 Integer featureSize)
    {
        this.epsilon = epsilon;
        this.featureSize = featureSize;
        study(nanoTagedAbstracts, markedAbstracts);
    }


    public NanoArticleClassifiar(List<String> nanoTagedAbstracts,
                                 List<AbstractRivendell> markedAbstracts)
    {
        this.epsilon = EPSILON;
        this.featureSize = FEATURE_SIZE;
        study(nanoTagedAbstracts, markedAbstracts);
    }

    // get functions //

    public double getEpsilon()
    {
        return this.epsilon;
    }

    public double getFeatureSize()
    {
        return this.featureSize;
    }

    // end - get functions //

    // logic functions //

    /**
     * build the module of the classifier
     */
    private void study(List<String> nanoTagedAbstracts, List<AbstractRivendell> markedAbstracts)
    {
        final int ITERATION = 1000;
        try
        {
            // create vocabulary to work with
            NlpClassifierPreprocessing preprocessing = new NlpClassifierPreprocessing(nanoTagedAbstracts);
            List<List<Integer>> signals = new ArrayList<>();
            for (AbstractRivendell markedAbstract : markedAbstracts)
            {
                signals.add(preprocessing.textToSignals(markedAbstract.getText()));
            }
            // get signals of words
            List<WordSignal> global = preprocessing.globalSignal(signals);
            // sort from biggest to smallest counts
            Collections.sort(global);
            Collections.reverse(global);

            // take only the leading signals
            this.leadingSignals = new ArrayList<>();
            Integer runCount = this.featureSize;
            if (global.size() < this.featureSize)
            {
                runCount = global.size();
            }
            for (int signalIndex = 0; signalIndex < runCount; ++signalIndex)
            {
                leadingSignals.add(global.get(signalIndex).getWord());
            }

            // get data ready to model
            List<LogisticRegression.Instance> goodBadSampels = splitToLabels(markedAbstracts, preprocessing, this.leadingSignals);
            // open model and train it
            this.model = new LogisticRegression(runCount, ITERATION, epsilon);
            this.model.train(goodBadSampels);
            logger.addLineToLog("study is finished with " + markedAbstracts.size() + " samples");
        }
        catch (Exception error)
        {
            logger.addLineToLog("Error at study from NanoArticleClassifiar - Can not study cause: " + error.getMessage());
        }
    }

    /**
     * Filter nano according to the bayes model
     * @param allAbstracts
     * @return
     */
    public List<AbstractRivendell> filterNano(List<AbstractRivendell> allAbstracts)
    {
        final double TRASHOLD = 0.5;
        try
        {
            // init answer
            int sizeBeforeFilter = allAbstracts.size();
            List<AbstractRivendell> answer = new ArrayList<>();
            // prepare corpus
            NlpClassifierPreprocessing pp = NlpClassifierPreprocessing.load(this.leadingSignals);
            // run on all the abstracts we have and filter the only ones we think is nano related
            for (int i = 0; i < allAbstracts.size(); i++)
            {
                // if not nano related
                if(this.model.classify(pp.textToSignals(allAbstracts.get(i).getText())) < TRASHOLD)
                {
                    allAbstracts.remove(i--);
                }
            }
            logger.addLineToLog("filter is started with " + sizeBeforeFilter + " and finished with " + allAbstracts.size());
            return allAbstracts;
        }
        catch (Exception error)
        {
            logger.addLineToLog("Error at filterNano from NanoArticleClassifiar with sub error: " + error.getMessage());
            // return empty list
            return new ArrayList<>();
        }
    }

    // end - logic functions //

    // help functions //

    private List<LogisticRegression.Instance> splitToLabels(List<AbstractRivendell> samples, NlpClassifierPreprocessing pp, List<String> leadingSignals)
    {
        // init answer
        List<LogisticRegression.Instance> answer = new ArrayList<>();
        // update corpus to leading signals
        pp.setCorpus(leadingSignals);
        // convert each abstract to learning DS
        for (AbstractRivendell simple: samples)
        {
            int label = 0; // not nano label
            if (simple.getIsNano())
            {
                label = 1; // nano label
            }
            answer.add(new LogisticRegression.Instance(1, pp.textToSignals(simple.getText())));
        }
        return answer;
    }

    // end - help functions //
}
