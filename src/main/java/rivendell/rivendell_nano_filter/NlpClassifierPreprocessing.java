package rivendell.rivendell_nano_filter;

import opennlp.tools.stemmer.PorterStemmer;
import utils.Pair;

import java.util.*;

/*
    the process we wish to do before running any kind of nlp binary classifier
 */
public class NlpClassifierPreprocessing
{
    // members //
    private ArrayList<String> corpus;
    // end - members //

    // consts //
    private static String DEFAULT_DIVIEDER = " ";
    // end - consts //

    // empty constractor
    private NlpClassifierPreprocessing()
    {

    }

    // full constractor
    public NlpClassifierPreprocessing(List<String> sources, String divider)
    {
        createVocabulary(sources, divider);
    }

    // default constractor
    public NlpClassifierPreprocessing(List<String> sources)
    {
        createVocabulary(sources, NlpClassifierPreprocessing.DEFAULT_DIVIEDER);
    }

    // building the object according to flags from different constractors
    private void createVocabulary(List<String> sources, String divider)
    {
        // build
        this.corpus = new ArrayList<String>();
        // add from sources all different words
        for (String source: sources)
        {
            this.corpus.addAll(NlpClassifierPreprocessing.splitText(source, divider));
        }
        // clear duplicates
        this.corpus = NlpClassifierPreprocessing.removeDuplicates(this.corpus);
        // order them
        Collections.sort(this.corpus);
    }

    //////////////////////////
    // convertion functions //
    //////////////////////////

    // convert test to signals of vectors
    public List<Integer> textToSignals(String text)
    {
        // fix test to format
        List<String> words = NlpClassifierPreprocessing.textToVector(text);
        // store size just not to call it each time
        int size = this.corpus.size();
        // build answer
        List<Integer> answer = new ArrayList<>();
        // init anything to 0
        for (int i = 0; i < size; ++i)
        {
            answer.add(0);
        }
        // put each signal as true
        for (String word: words)
        {
            for (int signalIndex = 0; signalIndex < size; ++signalIndex)
            {
                if (word.equals(this.corpus.get(signalIndex)))
                {
                    answer.set(signalIndex, answer.get(signalIndex)+1);
                    break;
                }
            }
        }
        return answer;
    }

    // combine all signals to single big signal
    public List<WordSignal> globalSignal(List<List<Integer>> signals)
    {
        List<WordSignal> answer = new ArrayList<>();
        // add all words to answer
        for (String word : this.corpus)
        {
            answer.add(new WordSignal(word, 0));
        }
        // count each apperance
        for (List<Integer> signalList: signals)
        {
            for (int innerListIndex = 0; innerListIndex < signalList.size(); ++innerListIndex)
            {
                answer.get(innerListIndex).increase(signalList.get(innerListIndex));
            }
        }
        return answer;
    }

    ////////////////////////////////
    // end - convertion functions //
    ////////////////////////////////

    ////////////////////
    // help functions //
    ////////////////////

    // convert the string to vector we can work with
    private static List<String> textToVector(String text)
    {
        ArrayList<String> answer = NlpClassifierPreprocessing.splitText(text, NlpClassifierPreprocessing.DEFAULT_DIVIEDER);
        // answer = NlpClassifierPreprocessing.removeDuplicates(answer);
        Collections.sort(answer);
        return dictineryRedaction(answer);
    }

    // from a string to words
    private static ArrayList<String> splitText(String source, String divider)
    {
        // standrad the string
        source = source.trim().toLowerCase();
        // clear double split
        String double_divider = divider + divider;
        while (source.contains(double_divider))
        {
            source = source.replace(double_divider, DEFAULT_DIVIEDER);
        }
        // clear anything not words
        source = source.replaceAll("[^A-Za-z]", "");
        // split
        return  (ArrayList<String>)Arrays.asList(source.split(divider));
    }

    // Function to remove duplicates from an ArrayList
    private static <T> ArrayList<T> removeDuplicates(ArrayList<T> list)
    {
        // Create a new LinkedHashSet
        // Add the elements to set
        Set<T> set = new LinkedHashSet<>(list);
        // Clear the list
        list.clear();
        // with no duplicates to the list
        list.addAll(set);
        // return the list
        return list;
    }

    // change each word to standard form
    private static ArrayList<String> dictineryRedaction(ArrayList<String> words)
    {
        // consts
        String PROBLEM_SPLIT_CHAR = "-";
        // init porter
        PorterStemmer stemmer = new PorterStemmer();

        for (int i = 0; i < words.size(); i++)
        {
            // this is necessary because SEMEVAL tokenizes in a different
            // way, using not only spaces but also hyphens to separate tokens
            if (!words.get(i).contains(PROBLEM_SPLIT_CHAR))
            {
                words.set(i, stemmer.stem(words.get(i)));
            }
            else
            {
                String[] subtokens = words.get(i).split(PROBLEM_SPLIT_CHAR);
                for (int j = 0; j < subtokens.length; j++)
                {
                    subtokens[j] = stemmer.stem(subtokens[j]);
                }
                words.set(i, String.join(PROBLEM_SPLIT_CHAR,subtokens));
            }
        }
        return words;
    }

    public NlpClassifierPreprocessing setCorpus(List<String> words)
    {
        this.corpus.clear();
        for (String word: words)
        {
            this.corpus.add(word);
        }
        return this;
    }

    public static NlpClassifierPreprocessing load(List<String> words)
    {
        return new NlpClassifierPreprocessing().setCorpus(words);
    }

    //////////////////////////
    // end - help functions //
    //////////////////////////

}
