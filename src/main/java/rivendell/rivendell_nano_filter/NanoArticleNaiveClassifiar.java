package rivendell.rivendell_nano_filter;

import rivendell.AbstractRivendell;
import utils.Logger.LoggerConsole;
import utils.Math_Nlp.TextToVec;
import utils.ml.naive_bayes.BayesClassifier;
import utils.ml.naive_bayes.Classifier;

import java.util.ArrayList;
import java.util.List;

/*
    gets a list of abstracts, a set of queries and relevent results from Rivendell
    and return the results we have that we think is close enough

    1. We use Logistic regression as a binary classification for raw data
 */
public class NanoArticleNaiveClassifiar
{
    // Logger //
    public static LoggerConsole logger = new LoggerConsole();
    // end - Logger //

    // consts //
    public static final String CATEGORY_NEGATIVE = "neg";
    public static final String CATEGORY_POSITIVE = "pos";
    // end - consts //

    // members //
    private double epsilon = 0.001;
    private Classifier<String, String> bayes;
    // end - members //

    // full constractor
    public NanoArticleNaiveClassifiar(List<AbstractRivendell> markedAbstracts, double epsilon)
    {
        bayes = new BayesClassifier<String, String>();
        this.epsilon = epsilon;
        study(markedAbstracts);
    }

    // get functions //

    public double getEpsilon()
    {
        return epsilon;
    }

    // end - get functions //

    // logic functions //

    /**
     * build the module of the classifier
     */
    private void study(List<AbstractRivendell> markedAbstracts)
    {
        try{
            // run each abstract and learn it
            int size = markedAbstracts.size();
            for (int i = 0; i < size; i++)
            {
                String label = CATEGORY_NEGATIVE;
                if (markedAbstracts.get(i).getIsNano()) { label = CATEGORY_POSITIVE;}
                bayes.learn(label, TextToVec.getRepresentiveWords(markedAbstracts.get(i).getText()));
            }
            logger.addLineToLog("study is finished with " + size + " samples and with " + bayes.getCategories().size() + " categories");
        }
        catch (Exception error)
        {
            logger.addLineToLog("Error at study from NanoArticleClassifiar - Can not study cause: " + error.getMessage());
        }
    }

    /**
     * Filter nano according to the bayes model
     * @param allAbstracts
     * @return
     */
    public List<AbstractRivendell> filterNano(List<AbstractRivendell> allAbstracts)
    {
        try
        {
            int sizeBeforeFilter = allAbstracts.size();
            List<AbstractRivendell> answer = new ArrayList<>();
            // run on all the abstracts we have and filter the only ones we think is nano related
            for (int i = 0; i < allAbstracts.size(); i++)
            {
                // if not nano related
                if(bayes.classify(TextToVec.getRepresentiveWords(allAbstracts.get(i).getText())).getCategory().equals(CATEGORY_NEGATIVE))
                {
                    allAbstracts.remove(i--);
                }
            }
            logger.addLineToLog("filter is started with " + sizeBeforeFilter + " and finished with " + allAbstracts.size());
            return allAbstracts;
        }
        catch (Exception error)
        {
            logger.addLineToLog("Error at filterNano from NanoArticleClassifiar with sub error: " + error.getMessage());
            // return empty list
            return new ArrayList<>();
        }
    }

    // end - logic functions //
}
