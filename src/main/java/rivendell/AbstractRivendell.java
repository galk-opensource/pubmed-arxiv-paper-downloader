package rivendell;

import rivendell.rivendell_nano_filter.NanoArticleClassifiar;
import utils.Logger.LoggerConsole;

import java.util.ArrayList;
import java.util.List;

/*
    This class holds the data for the abstracts rivendell provide
 */
public class AbstractRivendell
{
    // Logger //
    public static LoggerConsole logger = new LoggerConsole();
    // end - Logger //

    // consts //
    public static int EMPTY_SCORE = 0;
    public static int RELEVENT = 1;
    public static int UNKNOWN = 0;
    public static int NOT_RELEVENT = 2;
    public static int ALL = -1;

    // from string to object
    private static String SINGLE_OBJECT_SPLITER = "!-!";
    private static String OBJECT_PROPERTY_SPLITER = "&&&";
    private static String AUTHER_SPLITER = ";;;";
    private static String PROPERTY_SPLITER = "=";
    public static String NO_ABSTRACTS_SPLITER = "-!-!-!-!-!-";
    private static Integer PROPERTY_LEN = 3;
    // end - consts //

    // members //
    private String title;
    private String text;
    private String[] authors;
    private Boolean isNano;
    // end - members //

    // full costractor
    public AbstractRivendell(String newText,  String title, String[] authors, Boolean isNano)
    {
        fulfil(newText, title, authors);
        this.isNano = isNano;
    }

    // empty tagging constractor
    public AbstractRivendell(String newText,  String title, String[] authors)
    {
        fulfil(newText, title, authors);
        this.isNano = false;
    }

    private void fulfil(String newText,  String title, String[] authors)
    {
        this.text = newText;
        this.title = title;
        this.authors = authors;
    }

    // get functions //

    public String getTitle() {
        return title;
    }

    public String getText() {
        return text;
    }

    public String[] getAuthors() {
        return authors;
    }

    public Boolean getIsNano()
    {
        return this.isNano;
    }

    // end - get functions //

    // toString //

    public String toString()
    {
        return "{\"title\":\"" + this.title.replace("\"", "") +
                "\", \"text\":\"" + this.text.replace("\"", "") +
                "\", \"authors\":\"" + authorsToString() + "\"}";
    }

    private String authorsToString()
    {
        String s = "[";
        if (this.authors.length != 0)
        {
            for (int i = 0; i < this.authors.length - 1; i++)
            {
                s += "\"" + removeProblemChar(this.authors[i]) + "\", ";
            }
            s += "\"" + this.authors[this.authors.length - 1] + "\"";
        }
        return (s + "]");
    }

    private String removeProblemChar(String str)
    {
        final String PROBLEM_CHAR = "\'";
        return str.replace(PROBLEM_CHAR, "");
    }

    // end - toString //

    // convertion function //
    public static List<AbstractRivendell> toObject(String data)
    {
        return toObject(data, false);
    }

    public static List<AbstractRivendell> toObject(String data, boolean isNano)
    {
        // if we can not handle with this data string
        final int MIN_CHARS_TO_LAGIT_STRING = 20;
        if (data == null || data.length() < MIN_CHARS_TO_LAGIT_STRING)
        {
            return  new ArrayList<>();
        }

        // clear the "[]" in the end and start
        data = data.substring(1, data.length()-1);
        String[] singleObjectData = data.split(SINGLE_OBJECT_SPLITER);

        // go over each one and convert it
        List<AbstractRivendell> answer = new ArrayList<>();
        for (int i = 0; i < singleObjectData.length; i++)
        {
            AbstractRivendell holder = toSingleObject(singleObjectData[i], isNano);
            // make sure we get result before adding
            if(holder != null)
            {
                answer.add(holder);
            }
        }
        return answer;
    }

    /**
     * convert a string in a known format to an Abstract Object
     * @param data - the string with the data
     * @param isNano - if the abstract is nano or not (not given inside the string)
     * @return - the data of the string as object
     */
    public static AbstractRivendell toSingleObject(String data, boolean isNano)
    {
        data = data.substring(1, data.length()-1);
        String[] property = data.split(OBJECT_PROPERTY_SPLITER);
        if(property.length == PROPERTY_LEN)
        {
            try
            {
                String[] authers = property[2].substring(1, property[2].length()-1).split(AUTHER_SPLITER);
                return new AbstractRivendell(property[0].split(PROPERTY_SPLITER)[1], property[1].split(PROPERTY_SPLITER)[1], authers, isNano);
            }
            catch (Exception ex)
            {
                return null; // if some split does not work
            }
        }
        else
        {
            return null;
        }
    }

    // end - convertion function //

}
