package downloader;

import org.apache.commons.io.FileUtils;
import utils.Logger.LoggerConsole;
import utils.Pair;
import utils.file_handler.FileHandler;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.HashSet;
import java.util.List;
import java.util.ArrayList;

/*
    This class finds the pdf links in a web page and return them
 */
public class pdfDownloader
{
    // Logger //
    public static LoggerConsole logger = new LoggerConsole();
    // end - Logger //

    // consts //
    private static final String START_LINK = "<a";
    private static final String END_LINK = "</a";
    private static final String PDF_FORMAT = "pdf";
    private static final String HREF_START = "href=\"";
    private static final String HREF_END = "\"";

    private static final int KILO = 1024;
    private static final int MIN_PDF_SIZE = 100;
    // end - consts //

    // shadow empty constractor
    private pdfDownloader()
    {

    }

    /**
     * save url to destenation file - skip existing if configured returns false
     * if no exception or skip returns true
     * @param sourceUrl
     * @param destenationFile
     * @param skipAlreadyExistingFiles
     * @return
     * @throws IOException
     */
    public static boolean saveToFile(URL sourceUrl, File destenationFile, boolean skipAlreadyExistingFiles)
    {
        // Skip save if configured to skip existing and file exist
        if (skipAlreadyExistingFiles && (destenationFile.exists()))
        {
            logger.addLineToLog("Skiped file with path: " + destenationFile);
            return false;
        }

        try
        {
            FileUtils.copyURLToFile(sourceUrl, destenationFile);
            if (destenationFile.length() / KILO > MIN_PDF_SIZE) {
                logger.addLineToLog("pdfDownloader save file with file folder " + destenationFile.toString() + " from url " + sourceUrl.toString());
                return true;
            }
            else
            {
                // bad file - delete it
                FileHandler.deleteFile(destenationFile.getAbsolutePath());
                throw new Exception("File's size is too small - it probebly an error, but can be just small file");
            }
        }
        catch (Exception ex)
        {
            logger.addLineToLog("Error in saveToFile at pdfDownloader with: " + ex.getMessage());
            return false;
        }
    }

    public static boolean saveToFile(URL sourceUrl, File destenationFile)
    {
        return saveToFile(sourceUrl, destenationFile, false);
    }

    /**
     * This function gets a list of links and return for each link the PDF download likes for it
     * @param links - the sites with the PDF links to download
     * @return - list of site page and all the pdf links inside it
     */
    public static List<Pair<String, List<String>>> getPdfLinkToDomain(List<Pair<String, String>> links)
    {
        Iselenium iselenium = new Iselenium();
        List<Pair<String, List<String>>> answer = new ArrayList<>();

        //run over each html and run it
        int size = links.size();
        for (int i = 0; i < size; i++)
        {
            String linkHolder = links.get(i).getFirst();
            // we know arxiv is the pdf itself
            if (!linkHolder.startsWith("https://arxiv.org"))
            {
                logger.addLineToLog("Link " + linkHolder + " is asked to find links");
                String html = iselenium.getHtmlFromUrl(linkHolder);
                answer.add(new Pair<String, List<String>>(linkHolder, getPdfLink(html)));
            }
            else // set the link to the file to be the given link itself
            {
                logger.addLineToLog("Link " + linkHolder + " is Arxiv so itself the link");
                List<String> tempArxiv = new ArrayList<>();
                tempArxiv.add(linkHolder);
                answer.add(new Pair<String, List<String>>(linkHolder, tempArxiv));
            }
        }
        return answer;
    }

    public static List<String> getPdfLink(String html)
    {
        List<String> links = extractLinks(html);
        // go over all of them and search for pdfs
        for (int i = 0; i < links.size(); i++)
        {
            // if not pdf file link
            if (!links.get(i).contains(PDF_FORMAT))
            {
                // delete it
                links.remove(i--);
            }
        }
        links = getHref(links);
        return new ArrayList(new HashSet(links));
    }

    private static List<String> extractLinks(String html)
    {
        LoggerConsole logger = new LoggerConsole();
        final ArrayList<String> result = new ArrayList<String>();

        // run until you do not see a link
        while (html.contains(START_LINK))
        {
            String holderLink = "";
            // find the href inside data
            int start =  html.indexOf(START_LINK);
            int end = start + html.substring(start+1).indexOf(END_LINK);
            // put the link
            result.add(html.substring(start, end));
            // cut this part and go to next result
            html = html.substring(end);
        }
        return result;
    }

    private static List<String> getHref(List<String> links)
    {
        LoggerConsole logger = new LoggerConsole();
        final ArrayList<String> result = new ArrayList<String>();

        // run until you do not see a link
        int size = links.size();
        for (int i = 0; i < size; i++)
        {
            // check if we have link at all
            if (links.get(i).contains(HREF_START))
            {
                String holder = links.get(i);
                int start =  holder.indexOf(HREF_START) + HREF_START.length();
                int end = start + holder.substring(start).indexOf(HREF_END);
                // put the link
                result.add(holder.substring(start, end));
                logger.addLineToLog("href = " + result.get(result.size()-1));
            }
        }
        return result;
    }

}
