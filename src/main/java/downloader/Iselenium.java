package downloader;

import org.apache.commons.lang3.SystemUtils;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import utils.Logger.LoggerConsole;


/*
This is the interface for easy use of the Selenium library
 */
public class Iselenium
{
    // Logger //
    public static LoggerConsole logger = new LoggerConsole();
    // end - Logger //

    // consts //
    private static final int TIME_LOOP = 500;
    private static String DRIVER_PATH = SystemUtils.IS_OS_WINDOWS ?
            "src\\main\\resources\\chromedriver.exe" :
            "src/main/resources/chromedriver";
    private static String LOGGER_FILE;
    // end - consts //
    // members //
    private WebDriver driver;

    // end members //

    // full constractor
    public Iselenium(String driverLocation)
    {
        System.setProperty("webdriver.chrome.driver", driverLocation);
        fulfil();
    }

    // defualt constractor
    public Iselenium()
    {
        System.setProperty("webdriver.chrome.driver", DRIVER_PATH);
        fulfil();
    }

    private void fulfil()
    {
        ChromeOptions chromeOptions = new ChromeOptions();
        chromeOptions.addArguments("--headless");
        driver = new ChromeDriver(chromeOptions);
        logger.addLineToLog("Selinium Instance Started");
    }

    // input: page url
    // output: page html
    // action: download a page code using selenium web driver
    public String getHtmlFromUrl(String url, int waitSeconds, String elementHTMLinCode)
    {
        // log call
        logger.addLineToLog("Selinium called " +  url + " and waited " + waitSeconds + " seconds");

        final int TO_MILISECOND = 1000;

        driver.get(url);
        if(waitSeconds > 0)
        {
            try
            {
                Thread.sleep(waitSeconds * TO_MILISECOND );
            }
            catch (InterruptedException ex)
            {
                logger.addLineToLog("Error in getHtmlFromUrl at Iselenuim with: " + ex.getMessage());
                ex.printStackTrace();
            }
            /*
            await().atMost(waitSeconds, TimeUnit.SECONDS)
                    .pollDelay(TIME_LOOP, TimeUnit.MILLISECONDS)
                    .ignoreExceptions()
                    .until(() -> driver.getPageSource().contains(elementHTMLinCode));
            */
        }
        return driver.getPageSource();
    }

    // input: page url
    // output: page html
    // action: download a page code using selenium web driver
    public String getHtmlFromUrl(String url)
    {
        return getHtmlFromUrl(url, 0, null);
    }

    public void quit()
    {
        driver.quit();
        logger.addLineToLog("Selenium Thread Closed");
    }
}
