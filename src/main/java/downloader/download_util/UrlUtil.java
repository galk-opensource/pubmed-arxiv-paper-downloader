package downloader.download_util;

import utils.Logger.LoggerConsole;

import java.net.*;
import java.io.*;
import java.util.ArrayList;
import java.util.List;

/*
    This class manage the url operations
 */
public class UrlUtil
{
    // Logger //
    public static LoggerConsole logger = new LoggerConsole();
    // end - Logger //

    // consts //
    private static String WWW = "www";
    private static String URL_PROTOCOL = "http";
    private static String PROTOCOL_DOMAIN_SPLITER = "://";
    // end - consts //

    // members //

    // end - members //

    // shadow constractor
    private UrlUtil()
    {

    }

    // logic functions //

    public static List<String> getAbsultPath(String notClearDoamin, List<String> path)
    {
        List<String> answer = new ArrayList<>();
        // run over each and every one of the pathes
        for (int i = 0; i < path.size(); i++)
        {
            // get right link for this domain
            answer.add(getAbsultPath(notClearDoamin, path.get(i)));
        }
        return answer;
    }

    public static String getAbsultPath(String notClearDoamin, String path)
    {
        // make standard string
        path = path.trim().toLowerCase();
        try
        {
            // convert to easy to work format
            URL notClearDomain = new URL(notClearDoamin);

            // if protocol is missing
            if (path.startsWith(WWW))
            {
                return notClearDomain.getProtocol() + PROTOCOL_DOMAIN_SPLITER + path;
            }
            else if(path.startsWith(URL_PROTOCOL)) // if nothing is missing
            {
                return path;
            }
            else // if (path is relative)
            {
                return notClearDomain.getProtocol() + PROTOCOL_DOMAIN_SPLITER + notClearDomain.getHost() + path;
            }
        }
        catch (Exception ex)
        {
            logger.addLineToLog("Error in getAbsultPath at UrlUtil with: " + ex.getMessage());
            return null;
        }
    }

    // end - logic functions //
}
