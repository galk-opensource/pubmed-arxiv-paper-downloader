package downloader.download_util;

import utils.Logger.LoggerConsole;
import utils.Pair;

import javax.net.ssl.HttpsURLConnection;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;

/*
    Handle with Http requests and responses
 */
public class HttpHandler
{
    // Logger //
    public static LoggerConsole logger = new LoggerConsole();
    // end - Logger //

    // consts //
    private static final String USER_AGENT = "Mozilla/5.0";
    // end - consts //

    // shadow constractor
    private  HttpHandler()
    {

    }

    public static String sendGet(String url) throws Exception
    {
        URL obj = new URL(url);
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();
        logger.addLineToLog("Send Get to: " + obj.toString());

        // optional default is GET
        con.setRequestMethod("GET");

        //add request header
        con.setRequestProperty("User-Agent", USER_AGENT);
        return readResponce(con);
    }


    public static String sendGetWithHeader(String url, String headerValue, List<Pair<String, String>> parameters) throws Exception
    {
        URL obj = new URL(url + "?" + buildParameterString(parameters));
        return sendGetWithHeader(obj.toString(), headerValue);
    }

    public static String sendGetWithHeader(String url, String headerValue) throws Exception
    {
        // consts
        final int WAIT_TIME_IN_MILI_SECONDS = 180000;
        URL obj = new URL(url);
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();
        logger.addLineToLog("Send Get to: " + obj.toString() + " with header: " + headerValue);

        // optional default is GET
        con.setRequestMethod("GET");
        con.setReadTimeout(WAIT_TIME_IN_MILI_SECONDS); // wait up to 3 min cause we have performance problem in Rivendell

        //add request header
        con.setRequestProperty("User-Agent", headerValue);
        return readResponce(con);
    }

    // HTTP GET request
    public static String sendGet(String url, List<Pair<String, String>> parameters) throws Exception
    {
        URL obj = new URL(url + "?" + buildParameterString(parameters));
        return sendGet(obj.toString());
    }

    // HTTP POST request
    public static String sendPost(String url, List<Pair<String, String>> parameters) throws Exception
    {
        URL obj = new URL(url);
        HttpsURLConnection con = (HttpsURLConnection) obj.openConnection();
        logger.addLineToLog("Send POST to: " + obj.toString());

        //add reuqest header
        con.setRequestMethod("POST");
        con.setRequestProperty("User-Agent", USER_AGENT);
        con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");

        String urlParameters = buildParameterString(parameters);

        // Send post request
        con.setDoOutput(true);
        DataOutputStream wr = new DataOutputStream(con.getOutputStream());
        wr.writeBytes(urlParameters);
        wr.flush();
        wr.close();

        return readResponce(con);
    }

    // build the string from the parameters data
    private static String buildParameterString(List<Pair<String, String>> parameters)
    {
        if (parameters != null && parameters.size() > 0)
        {
            String answer = "";
            int size = parameters.size();
            // run over all and build each part
            for (int i = 0; i < size - 1; i++)
            {
                answer += parameters.get(i).getFirst() + "=" + parameters.get(i).getSecond() + "&";
            }
            answer += parameters.get(size - 1).getFirst() + "=" + parameters.get(size - 1).getSecond();
            return answer;
        }
        return ""; // error - empty parameters
    }

    ////////////////////
    // help functions //
    ////////////////////

    public static String readResponce(HttpURLConnection connection) throws IOException
    {
        BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
        String inputLine;
        StringBuffer response = new StringBuffer();

        while ((inputLine = in.readLine()) != null)
        {
            response.append(inputLine);
        }
        in.close();

        return response.toString();
    }

    //////////////////////////
    // end - help functions //
    //////////////////////////

}
