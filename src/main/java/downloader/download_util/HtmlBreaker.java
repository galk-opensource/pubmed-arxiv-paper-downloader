package downloader.download_util;

import utils.Logger.LoggerConsole;
import utils.Pair;

import java.util.ArrayList;
import java.util.List;

/*
This class contains usful function to break html elements and get there useful data
 */
public class HtmlBreaker
{
    // Logger //
    public static LoggerConsole logger = new LoggerConsole();
    // end - Logger //

    // input: link text or part of it and the page code
    // output: all links of the <a></a> stands the link text
    // action: break each <a> elements and get from it the href attribute
    public static List<String> getLinkByText(String linkPre, String linkPost, String linkText,
                                             String hrefPre, String hrefPost, String hrefText, String html)
    {
        Pair<String, Integer> link;
        List<String> linksHTML = new ArrayList<String>();
        // find next link with this text
        do
        {
            // break the link string from the html
            link = HtmlBreaker.breakString(html, linkPre, linkPost, linkText);
            // make sure we found any result at all
            if (link != null)
            {
                // the link is found and right one
                if (link.getFirst() != null)
                {
                    Pair<String, Integer> holder = HtmlBreaker.breakString(link.getFirst(),hrefPre, hrefPost, hrefText);
                    if (holder != null && holder.getFirst() != null)
                    {
                        linksHTML.add(holder.getFirst()); // put the text in the list
                    }
                }
                // go to the next part of the string to search
                if (link.getSecond() <= 0)
                {
                    if (html.indexOf(hrefText) > 0)
                    {
                        html = html.substring(html.indexOf(hrefText));
                    }
                    else
                    {
                        html = html.substring(1);
                    }
                }
                else
                {
                    html = html.substring(link.getSecond());
                }
            }
        }
        while (link != null);
        return linksHTML;
    }

    // input: string and 2 sub strings which we want to take between in the first string
    // output: the string between the 2 substrings
    // action: clear all before the first (including), save until the second
    public static Pair<String, Integer> breakString(String html, String start, String end, String contains)
    {
        final int ERROR_NOT_FOUND = -1;

        int indexFirst = html.indexOf(start);
        // if we can not find the start so the process wont work anyway
        if (indexFirst < 0 || indexFirst + start.length() > html.length())
        {
            logger.addLineToLog("Error with finding in html start: " + start + " ,end: " + end);
            return null;
        }
        indexFirst += start.length();
        html = html.substring(indexFirst);
        int indexLast = html.indexOf(end);
        int indexCenter = html.indexOf(contains);

        // make sure the process is lagit
        if (indexFirst != ERROR_NOT_FOUND && indexLast != ERROR_NOT_FOUND
                && indexCenter != ERROR_NOT_FOUND && indexCenter < indexLast)
        {
            html = html.substring(0, indexLast);
            return new Pair<>(html, indexFirst + indexLast - start.length());
        }
        else if(indexFirst != -1 && indexLast != -1 && indexCenter > indexLast)
        {
            return new Pair<>(null, indexFirst + indexLast - start.length());
        }
        else
        {
            // else - error
            logger.addLineToLog("Error with finding in html start: " + start + " ,end: " + end + " ,contain: " + contains);
            return null;
        }
    }
}
