package downloader.download_util;

import rivendell.AbstractRivendell;
import rivendell.ArticleRivendell;
import rivendell.RivendellApi;
import utils.Logger.LoggerConsole;
import utils.Pair;

import java.util.ArrayList;
import java.util.List;

public class AbstractToArticleLink
{
    // Logger //
    public static LoggerConsole logger = new LoggerConsole();
    // end - Logger //

    private static String PMID_SPLIITER = "pmid";
    private static String PUBMED_SINGLE_ABSTRACT_URL = "https://www.ncbi.nlm.nih.gov/pubmed/?term=";
    private static String TEXT_BEFORE_ARTICLE_LINK = "Full Text Sources";
    private static String TEXT_BEFORE_ARTICLE_LINK_aspxCODE = "href=\"";
    private static String URL_STANDART_START = "http";

    public static List<Pair<ArticleRivendell, String>> getArticles(List<AbstractRivendell> releventAbstract, int db)
    {
        logger.addLineToLog("start get articles with " + releventAbstract.size() + " articles");
        List<Pair<ArticleRivendell, String>> articles = new ArrayList<>();

        Boolean isAll;
        List<String> indexs;
        String holder = "";

        if (db == RivendellApi.DB_PUBMED)
        {
            indexs = extractPmid(releventAbstract);
            for (int i = 0; i < indexs.size(); i++)
            {
                holder = getPageLinkSource(indexs.get(i));
                if (holder != null)
                {
                    articles.add(new Pair<ArticleRivendell, String>(new ArticleRivendell(holder, indexs.get(i)), releventAbstract.get(i).getTitle()));
                }
                else
                {
                    logger.addLineToLog("article " + indexs.get(i).toString() + " return null in PUBMED");
                }
            }
        }
        else if (db == RivendellApi.DB_ARXIV)
        {
            indexs = extractTitle(releventAbstract);
            for (int i = 0; i < indexs.size(); i++)
            {
                holder = getPdfFromSearch(indexs.get(i));
                if (holder != null)
                {
                    articles.add(new Pair<ArticleRivendell, String>(new ArticleRivendell(holder, indexs.get(i)), releventAbstract.get(i).getTitle()));
                }
                else
                {
                    logger.addLineToLog("article " + indexs.get(i).toString() + " return null in ARXIV");
                }
            }
        }
        else // if (db == RivendellApi.DB_ALL)
        {
            List<Pair<String,Boolean>> indicators = extractIndicator(releventAbstract);
            for (int i = 0; i < indicators.size(); i++)
            {
                // if pbumed
                if (indicators.get(i).getSecond())
                {
                    holder = getPageLinkSource(indicators.get(i).getFirst());
                    if (holder != null)
                    {
                        articles.add(new Pair<ArticleRivendell, String>(new ArticleRivendell(holder, indicators.get(i).getFirst()), releventAbstract.get(i).getTitle()));
                    }
                    else
                    {
                        logger.addLineToLog("article " + indicators.get(i).toString() + " return null in PUBMED");
                    }
                }
                else // if arxiv
                {
                    holder = getPdfFromSearch(indicators.get(i).getFirst());
                    if (holder != null)
                    {
                        articles.add(new Pair<ArticleRivendell, String>(new ArticleRivendell(holder, indicators.get(i).getFirst()), releventAbstract.get(i).getTitle()));
                    }
                    else
                    {
                        logger.addLineToLog("article " + indicators.get(i).toString() + " return null in ARXIV");
                    }
                }
            }
        }
        return articles;
    }

    private static List<Pair<String, Boolean>> extractIndicator(List<AbstractRivendell> abstracts)
    {
        // seperate results to 2 apis we use
        List<AbstractRivendell> pubmedResults = new ArrayList<>();
        List<AbstractRivendell> arxivResults = new ArrayList<>();

        // go over all the abstracts and put each one to the right packet
        for (int i = 0; i < abstracts.size(); i++)
        {
            // if pubmed
            if (abstracts.get(i).getText().contains(PMID_SPLIITER))
            {
                pubmedResults.add(abstracts.get(i));
            }
            else // if arxiv
            {
                arxivResults.add(abstracts.get(i));
            }
        }
        Boolean pubmedFlag, arxivFlag;
        int devider;
        List<String> answer = extractPmid(pubmedResults); // get pubmed articles
        devider = answer.size(); // save the first list count for devider later
        answer.addAll(extractTitle(arxivResults)); // add to them the arxiv articles
        return addBilongness(answer, devider, true);
    }

    // get pmid from the abstract text (which has pmid)
    private static List<String> extractPmid(List<AbstractRivendell> abstracts)
    {
        List<String> answer = new ArrayList<>();
        int counter = 0;

        for (int i = 0; i < abstracts.size(); i++)
        {
            if (abstracts.get(i).getText().contains(PMID_SPLIITER))
            {
                answer.add(abstracts.get(i).getText().substring(abstracts.get(i).getText().lastIndexOf(PMID_SPLIITER) + PMID_SPLIITER.length() + 1));
                counter++;
            }
        }
        return answer;
    }

    // get titel from the abstract object
    public static List<String> extractTitle(List<AbstractRivendell> abstracts)
    {
        List<String> answer = new ArrayList<>();
        for (int i = 0; i < abstracts.size(); i++)
        {
            answer.add(abstracts.get(i).getTitle());
        }
        return answer;
    }

    // get pmid from the abstract text (which has pmid)
    public static String extractPmid(String abstractPmid)
    {
        if (abstractPmid.contains(PMID_SPLIITER))
        {
            return abstractPmid.substring(abstractPmid.lastIndexOf(PMID_SPLIITER) + PMID_SPLIITER.length() + 1);
        }
        logger.addLineToLog("can not extract pmid from " + abstractPmid);
        return null;
    }

    // get pmid and return article link
    private static String getPageLinkSource(String pmid)
    {
        String answer = "";
        String aspx = "";

        // get the page data from linb
        try
        {
            aspx = HttpHandler.sendGet(PUBMED_SINGLE_ABSTRACT_URL + pmid);
        }
        catch (Exception ex)
        {
            logger.addLineToLog("Error in getPageLinkSource at AbstractToArticleLink with: " + ex.getMessage());
            return null;
        }

        int firstIndex = aspx.indexOf(TEXT_BEFORE_ARTICLE_LINK) + TEXT_BEFORE_ARTICLE_LINK.length();
        aspx = aspx.substring(firstIndex);
        firstIndex = aspx.indexOf(TEXT_BEFORE_ARTICLE_LINK_aspxCODE) + TEXT_BEFORE_ARTICLE_LINK_aspxCODE.length();

        int i = 0;
        while (aspx.charAt(firstIndex + i) != '\"')
        {
            answer += aspx.charAt(firstIndex + i++);
        }

        if (!answer.startsWith(URL_STANDART_START))
        {
            answer = null;
        }
        return answer;
    }

    // get pmid and return article link
    private static String getPdfFromSearch(String title)
    {
        // consts to work with the API URL
        final int SAFE_LINK_ZONE = 50;
        final String BASE_URL_ANSWER = "https://arxiv.org";
        final String BASE_URL = "https://arxiv.org/search";

        final String PDF_ANKER = ">pdf</a>";
        final String PDF_FILE_END_LINK = "/pdf/";
        final String PDF_FILE_START_LINK= "https://arxiv.org/";

        // download page code
        String html = arxivPost(BASE_URL, title.replace("\n", "").trim().replace("  ", " ").replace(" ", "%20"));

        // check if we found an answer
        if (html != null && html.contains(PDF_ANKER))
        {
            html = html.substring(html.indexOf(PDF_ANKER) - SAFE_LINK_ZONE);
            int first = html.indexOf(PDF_FILE_END_LINK) + PDF_FILE_END_LINK.length();
            int hrefLen = 0;
            // count the length
            for (int i = first + 1; i < html.length(); i++)
            {
                if (html.charAt(i) != '"')
                {
                    hrefLen++;
                }
                else
                {
                    break;
                }
            }
            return BASE_URL_ANSWER + PDF_FILE_END_LINK + html.substring(first, first + hrefLen + 1);
        }
        else
        {
            return null;
        }
    }

    public static String arxivPost(String url, String title)
    {
        try
        {
            List<Pair<String, String>> parameters = new ArrayList<>();
            parameters.add(new Pair<String, String>("searchtype", "all"));
            parameters.add(new Pair<String, String>("query", title));
            return HttpHandler.sendGetWithHeader(url, "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.186 Safari/537.36", parameters);
        }
        catch (Exception ex)
        {
            logger.addLineToLog("Error in arxivPost at AbstractToArticleLink with: " + ex.getMessage());
            return null;
        }
    }

    public static List<Pair<String, Boolean>> addBilongness(List<String> list, int deviderPlace, Boolean firstFlag)
    {
        List<Pair<String, Boolean>> answer = new ArrayList<>();
        for (int i = 0; i < list.size(); i++)
        {
            if (i < deviderPlace)
            {
                answer.add(new Pair<String, Boolean>(list.get(i), firstFlag));
            }
            else
            {
                answer.add(new Pair<String, Boolean>(list.get(i), !firstFlag));
            }
        }
        return answer;
    }
}
