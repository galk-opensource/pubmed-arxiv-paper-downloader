package utils;

import utils.Logger.LoggerConsole;

/*
This class holds pair of numbers to present range in a lagitimic order
 */
public class Range
{

    // Logger //
    public static LoggerConsole logger = new LoggerConsole();
    // end - Logger //

    // members //
    private Pair<Double, Double> range;
    // end - members //

    // constractor
    public Range(double newStart, double newEnd)
    {
        // find the right order of the range
        if(newStart > newEnd)
        {
            this.range = new Pair<Double, Double>(newEnd, newStart);
        }
        else
        {
            this.range = new Pair<Double, Double>(newStart, newEnd);
        }
    }

    public Range(String data)
    {
        // check that we can work with it
        if(data != null && data.length() > 6)
        {
            data = data.substring(1,data.length()-2);
            String[] values = data.split(", ");
            // if the right amount of elements
            if(values.length == 2)
            {
                try
                {
                    this.range = new Pair<Double, Double>(Double.parseDouble(values[0]), Double.parseDouble(values[1]));
                }
                catch (Exception ex)
                {
                    logger.addLineToLog("Error in constractor in Range with: " + ex.getMessage());
                    this.range = null;
                }
            }
        }
        else
        {
            logger.addLineToLog("Error in constractor in Range with: wrong string format to build a range object");
            this.range = null;
        }
    }

    // get functions //

    public double getStart()
    {
        return this.range.getFirst();
    }

    public double getEnd()
    {
        return this.range.getSecond();
    }

    // end get functions //

    // set functions //

    public void setStart(double newStart)
    {
        // make sure the range is lagit
        if(newStart <= this.getEnd())
        {
            this.range.setFirst(newStart);
        }
        logger.addLineToLog("Error in setStart in Range with: can not set value in start - " + newStart);
    }

    public void setEnd(double newEnd)
    {
        // make sure the range is lagit
        if(newEnd >= this.getStart())
        {
            this.range.setSecond(newEnd);
        }
        logger.addLineToLog("Error in setEnd in Range with: can not set value in start - " + newEnd);
    }
    // end set functions //

    public String toString()
    {
        return this.range.toString();
    }

    public boolean equals(Range other)
    {
        if(this.range.getFirst() == other.range.getFirst() &&
                this.range.getSecond() == other.range.getSecond())
        {
            return true;
        }
        return false;
    }
}
