package utils;

import utils.Logger.LoggerConsole;

import java.util.ArrayList;
import java.util.List;

/*
This class holds pair of types just for easy use
 */
public class Pair<T,P>
{
    // Logger //
    public static LoggerConsole logger = new LoggerConsole();
    // end - Logger //

    // members //
    private T first;
    private P second;
    // end - members //

    // constractor
    public Pair(T newFirst, P newSecond)
    {
        this.first = newFirst;
        this.second = newSecond;
    }

    // empty constractor
    public Pair()
    {
        this.first = null;
        this.second = null;
    }

    // get functions //

    public T getFirst()
    {
        return this.first;
    }

    public P getSecond()
    {
        return this.second;
    }

    // end get functions //

    // set functions //

    public void setFirst(T newFirst)
    {
        this.first = newFirst;
    }

    public void setSecond(P newSecond)
    {
        this.second = newSecond;
    }
    // end set functions //

    public String toString()
    {
        return "[" + first.toString() + ", " + second.toString() + "]";
    }

    // this function gets pair list and return "toString" list
    public static List<String> toStringList(List<Pair<String, Integer>> list)
    {
        List<String> answer = new ArrayList<String>();
        // go over the original list and convert each pair
        for (int i = 0; i < list.size(); i++)
        {
            answer.add(list.get(i).toString());
        }
        return answer;
    }
}
