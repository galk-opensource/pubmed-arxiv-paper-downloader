package utils.Math_Nlp;

import org.apache.poi.ss.formula.functions.T;
import utils.Logger.LoggerConsole;
import utils.Pair;
import utils.file_handler.txt_handler.TxtHandler;

import java.util.*;

/*
    This class manage the operations on text for easy to use
    NLP operation after it
 */
public class TextToVec
{
    // logger //
    private static LoggerConsole logger = new LoggerConsole();
    // end - logger //

    // consts //
    public static List<String> blackList;
    public static final String BlackListFilePath = "C:\\Users\\user\\Desktop\\nanolibrary\\src\\main\\resources\\blacklist.txt";
    // end - consts //

    public static List<String> getRepresentiveWords(String word) throws Exception
    {
        List<String> holder = new ArrayList<>();
        holder.add(word);
        return getRepresentiveWords(holder);
    }

    // get a list of strings and convent each of them to vectors and then make a single vector of unique words
    public static List<String> getRepresentiveWords(List<String> words) throws Exception
    {
        // build first time the blacklist
        if(blackList == null)
        {
            logger.addLineToLog("Black list words loaded ");
            blackList = loadBlackList();
        }

        List<String> abstractVectors = new ArrayList<>();
        // get all vectors into one of all the abstracts
        for (int wordIndex = 0; wordIndex < words.size(); wordIndex++)
        {
            abstractVectors.addAll(textToVector(words.get(wordIndex)));
        }

        // get unique words and without the ones in the black list
        List<String> uniqueAbstractVectors = removeBlackList(new ArrayList(new HashSet(abstractVectors)));

        // make sure we have only unique words
        List<Pair<String, Double>> countList = scoreVector(uniqueAbstractVectors, abstractVectors);

        // sort the list to most important ones
        countList = sort(countList);

        // clear things we not sure about them
        countList = clearNotSureValues(countList);

        // convert to text only again
        return fromPairToString(countList);
    }

    // input: an ordered list of couple of term and score
    // output: only the values much better then the score
    // action: get the values better then the AVG assuming we have *** skewed left normal probability ***
    //          so we wish to get only results more then 2
    public static List<Pair<String, Double>> clearNotSureValues(List<Pair<String, Double>> terms) throws Exception
    {
        // consts
        final int DEVERSION_LEVEL = 2;

        // there is an error
        if (terms == null || terms.size() == 0)
        {
            throw new Exception("Error at clearNotSureValues from TextToVec - input must be not null and with at least one element in list");
        }

        // find the AVG
        double avg = 0;
        // find the AVG^2
        double avgSqure = 0;
        for (Pair<String, Double> term: terms)
        {
            avg += term.getSecond();
            avgSqure += (term.getSecond() * term.getSecond());
        }
        avg /= terms.size();
        avgSqure /= terms.size();

        // find the Standard deviation
        double sd = Math.sqrt(avgSqure - Math.pow(avg, 2));

        // log the findings
        logger.addLineToLog("Avg of relevent words is: " + avg);
        logger.addLineToLog("Standard deviation of relevent words is: " + sd);

        // find which score is bigger then AVG + 2 SD
        // run over until you fail (this is legit cause the list is ordered)
        List<Pair<String, Double>> answer = new ArrayList<>();
        int wall = (int)Math.round(avg + DEVERSION_LEVEL * sd); // the score each trem need to pass
        for (int i = 0; i < terms.size(); i++)
        {
            // check if we pass the wall
            if (terms.get(i).getSecond() > wall)
            {
                answer.add(terms.get(i));
                continue; // jump to next one
            }
            break; // no results go to next one
        }
        // log results
        logger.addLineToLog("we started with " + terms.size() + " terms and pass with " + answer.size());
        return answer;
    }

    /**
     * give each word the number of times it presented
     * @param uniqe - uniqe words in the string
     * @param original - the original string as vector
     * @return - word + score list
     */
    public static List<Pair<String, Double>> scoreVector(List<String> uniqe, List<String> original)
    {
        // make sure we have only unique words
        List<Pair<String, Double>> countList = new ArrayList<>();
        for (int i = 0; i < uniqe.size(); i++)
        {
            countList.add(new Pair<String, Double>(uniqe.get(i), 0.0));
        }
        uniqe.clear(); // free this memory

        // count how many times we found each word
        for (int i = 0; i < countList.size(); i++)
        {
            for (int j = 0; j < original.size(); j++)
            {
                if (countList.get(i).getFirst().equals(original.get(j)))
                {
                    countList.get(i).setSecond(countList.get(i).getSecond() + 1);
                }
            }
        }
        logger.addLineToLog("scoreVector done on a uniqe list with " + countList.size() + " elements");
        return countList;
    }

    /**
     * remove from given vector of words the words we have in the black list
     * @param list
     * @return list without black list words
     */
    public static List<String> removeBlackList(List<String> list)
    {
        if (blackList != null) // make sure we found list
        {
            logger.addLineToLog("List with black words with " + list.size() + " elements ");
            // remove all the words we know not helping us
            for (int j = 0; j < blackList.size(); j++)
            {
                list.remove(blackList.get(j).trim());
            }

            for (int i = 0; i < list.size(); i++)
            {
                // if number in string then we do not need this word
                if(numberPredicate(list.get(i)))
                {
                    list.remove(i--);
                }
            }
            logger.addLineToLog("List after removing black words with " + list.size() + " elements ");
        }
        else
        {
            logger.addLineToLog("Did not change black words cause the list did not loaded");
        }
        return list;
    }

    /**
     * convert a string to list of words after clearing strange chars outside from them
     * @param text - string to convert
     * @return - list of words from the string
     */
    public static List<String> textToVector(String text)
    {
        text = text.toLowerCase().trim();
        // filter strange chars
        text = text.replace("-", " ").replace("(", " ").replace(")", " ");
        text = text.replace("[", " ").replace("]", " ");
        text = text.replace("{", " ").replace("}", " ");

        // make the slipt legit
        while (text.contains("  "))
        {
            text = text.replace("  ", " ");
        }
        return toList(text.split(" "));
    }

    /**
     * load the list of words we do not want from a data file
     * @return
     */
    private static List<String> loadBlackList()
    {
        return toList(TxtHandler.readAllText(BlackListFilePath).split(","));
    }

    /**
     * just convert an array to list
     * @param arr - array
     * @return list
     */
    private static List<String> toList(String[] arr)
    {
        List<String> answer = new ArrayList<>();
        if(arr != null && arr.length > 0)
        {
            // run over all the strings
            for (int i = 0; i < arr.length; i++)
            {
                // if not broken string
                if(arr[i] != null)
                {
                    answer.add(arr[i].trim());
                }
            }
        }
        return answer;
    }

    /**
     * convert a string + score back to string again
     * @param list - word + score list
     * @return just word list
     */
    private static List<String> fromPairToString(List<Pair<String, Double>> list)
    {
        List<String> answer = new ArrayList<>();
        if(list != null && list.size() > 0)
        {
            // run over all the strings
            for (int i = 0; i < list.size(); i++)
            {
                // if not broken string
                if(list.get(i) != null && list.get(i).getFirst() != null)
                {
                    answer.add(list.get(i).getFirst());
                }
            }
        }
        logger.addLineToLog("From Pair to String list with " + list.size() + " elements ");
        return answer;
    }

    /**
     * Sort the array using inner sort algo of JAVA
     * @param list - the array to sort
     * @return the sorted array
     */
    private static List<Pair<String, Double>> sort(List<Pair<String, Double>> list)
    {
        Collections.sort(list, new Comparator<Pair<String, Double>>()
        {
            public int compare(Pair<String, Double> lhs, Pair<String, Double> rhs)
            {
                // -1 - less than, 1 - greater than, 0 - equal, all inversed for descending
                return lhs.getSecond() > rhs.getSecond() ? -1 : (lhs.getSecond() < rhs.getSecond()) ? 1 : 0;
            }
        });
        logger.addLineToLog("Sort list with " + list.size() + " elements ");
        return list;
    }

    ////////////////////
    // help functions //
    ////////////////////

    private static Boolean numberPredicate(String word)
    {
        if (word.trim().replaceAll("[^0-9]", "").length() > 0)
        {
            return false;
        }
        return true;
    }

    //////////////////////////
    // end - help functions //
    //////////////////////////

}
