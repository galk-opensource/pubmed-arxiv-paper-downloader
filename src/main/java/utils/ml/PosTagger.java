package utils.ml;

import opennlp.tools.postag.POSModel;
import opennlp.tools.postag.POSTaggerME;
import opennlp.tools.sentdetect.SentenceDetector;
import opennlp.tools.sentdetect.SentenceDetectorME;
import opennlp.tools.sentdetect.SentenceModel;
import opennlp.tools.tokenize.WhitespaceTokenizer;
import utils.Logger.LoggerConsole;
import utils.Pair;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * using the OpenNLP libraray we are making an Part Of Speach to a text
 */
public class PosTagger
{
    // Logger //
    public static LoggerConsole logger = new LoggerConsole();
    // end - Logger //

    // members //
    POSTaggerME tagger = null;
    POSModel model = null;
    // end - members //

    // constractor
    public PosTagger(String learningFileData)
    {
        initialize(learningFileData);
    }

    public void initialize(String learningFileData)
    {
        try
        {
            InputStream modelStream =  getClass().getResourceAsStream(learningFileData);
            model = new POSModel(modelStream);
            tagger = new POSTaggerME(model);
        }
        catch (IOException ex)
        {
            logger.addLineToLog("Error at initialize in PosTagger with: " + ex.getMessage());
        }
    }

    public List<Pair<String, String>> tag(String text)
    {
        List<Pair<String, String>> answer = new ArrayList<>();
        try
        {
            if (model != null)
            {
                POSTaggerME tagger = new POSTaggerME(model);
                if (tagger != null)
                {
                    String[] sentences = detectSentences(text);
                    for (String sentence : sentences)
                    {
                        String whitespaceTokenizerLine[] = WhitespaceTokenizer.INSTANCE
                                .tokenize(sentence);
                        String[] tags = tagger.tag(whitespaceTokenizerLine);
                        for (int i = 0; i < whitespaceTokenizerLine.length; i++)
                        {
                            String word = whitespaceTokenizerLine[i].trim();
                            String tag = tags[i].trim();
                            answer.add(new Pair<String, String>(tag, word));
                        }
                    }
                    return answer;
                }
            }
        }
        catch (Exception ex)
        {
            logger.addLineToLog("Error at tag in PosTagger with: " + ex.getMessage());
        }
        logger.addLineToLog("Error at tag in PosTagger cause model is null or tagger is null");
        return null;
    }

    public String[] detectSentences(String paragraph) throws IOException {

        InputStream modelIn = getClass().getResourceAsStream("/en-sent.bin");
        final SentenceModel sentenceModel = new SentenceModel(modelIn);
        modelIn.close();

        SentenceDetector sentenceDetector = new SentenceDetectorME(sentenceModel);
        return sentenceDetector.sentDetect(paragraph);
    }
}
