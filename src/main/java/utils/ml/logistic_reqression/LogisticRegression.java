package utils.ml.logistic_reqression;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

/*
    A machine learning algorithm of binominal logis regrssion
 */
public class LogisticRegression
{
    // consts //
    private static double DEFUALT_RATE = 0.001;
    private static int DEFUALT_ITERATIONS = 1000;
    // end - consts //

    // members //
    /** the learning rate */
    private double rate;

    /** the weight to learn */
    private double[] weights;

    /** the number of iterations */
    private int iterations;
    // end - members //

    // full costractor
    public LogisticRegression(int numberOfWeights, int iterations, double rate) throws Exception
    {
        if (numberOfWeights > 0 && iterations > 0 && rate > 0)
        {
            this.iterations = iterations;
            this.rate = rate;
            init(numberOfWeights);
        }
        else
        {
            throw new Exception("Can not init LogisticRegression with negative or zero values");
        }
    }

    // default constractor
    public LogisticRegression(int numberOfWeights) throws Exception
    {
        if (numberOfWeights > 0)
        {
            this.rate = DEFUALT_RATE;
            this.iterations = DEFUALT_ITERATIONS;
            init(numberOfWeights);
        }
        else
        {
            throw new Exception("Can not init LogisticRegression with negative or zero values");
        }
    }

    // set weights to 0
    private void init(int numberOfWeights)
    {
        weights = new double[numberOfWeights];
        for (int i = 0; i < numberOfWeights; ++i)
        {
            weights[i] = 0;
        }
    }

    // train the model
    public void train(List<Instance> instances)
    {
        for (int n =0 ; n < this.iterations; ++n)
        {
            for (int i = 0; i < instances.size(); ++i)
            {
                List<Integer> x = instances.get(i).x;
                double predicted = classify(x);
                int label = instances.get(i).label;
                for (int j = 0; j < weights.length; ++j)
                {
                    this.weights[j] = this.weights[j] + this.rate * (label - predicted) * x.get(j);
                }
            }
        }
    }

    // predict function
    public double classify(List<Integer> x)
    {
        double logit = .0;
        for (int i = 0; i < this.weights.length; ++i)
        {
            logit += this.weights[i] * x.get(i);
        }
        return sigmoid(logit);
    }

    ////////////////////
    // help functions //
    ////////////////////

    private static double sigmoid(double z)
    {
        return 1.0 / (1.0 + Math.exp(-z));
    }

    public static class Instance {
        public int label;
        public List<Integer> x;

        public Instance(int label, List<Integer> x) {
            this.label = label;
            this.x = x;
        }
    }

    //////////////////////////
    // end - help functions //
    //////////////////////////


    ////////////////////
    // ioio functions //
    ////////////////////

    public double[] save()
    {
        return this.weights;
    }

    public void load(double[] newWeights)
    {
        this.weights = newWeights.clone();
    }

    ////////////////////////
    // end ioio functions //
    ////////////////////////
}
