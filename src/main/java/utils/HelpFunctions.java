package utils;

/**
 * hold different function which we can use in the system
 */
public class HelpFunctions {

    /**
     * is the string is number or not
     * @param value - the value string to convert
     * @return is the value is a number string or not
     */
    public static boolean tryParseInt(String value) {
        try {
            Integer.parseInt(value);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }
}
