package utils;

import utils.Logger.LoggerConsole;

import java.util.ArrayList;
import java.util.List;

/*
 Just handle with toString of some format to make things easy
 */
public class ListToString<T>
{

    // Logger //
    public static LoggerConsole logger = new LoggerConsole();
    // end - Logger //

    // consts //
    public static final String SPLITER = ",";
    // end - consts //

    public String toStringList(List<Object> list)
    {
        List<String> strs = new ArrayList<>();
        // go over all the elements of the list and convert to string
        int size = list.size();
        for (int i = 0; i < size; i++)
        {
            strs.add(((T)(list.get(i))).toString());
        }
        return toStringList(strs, SPLITER);
    }

    public static String toStringList(List<String> list, String devider)
    {
        // logger.addLineToLog("toString list with devider '" + devider + "' is asked over " + list.size() + " elements");

        String s = "";
        s += "[";
        if(!(list == null || list.size() == 0))
        {
            // make sure we can work with the devider
            if (!(devider != null && devider.length() > 0))
            {
                devider = SPLITER;
            }

            // go over all the elements in the list
            int size = list.size() - 1;
            for (int i = 0; i < size; i++)
            {
                s += list.get(i) + devider;
            }
            s += list.get(size);
        }
        s += "]";
        return s;
    }

    public static String toString2DimArray(String[][] data, String deviderRow, String deviderLine)
    {
        logger.addLineToLog("toString list with deviderRow '" + deviderRow +
                "' and with deviderLine = '" + deviderLine + "' is asked over " + data.length*data[0].length + " elements");
        String s = "";
        s += "[";
        if(!(data == null || data.length == 0))
        {
            // go over all the elements in the rows
            int sizeRows = data.length;
            for (int i = 0; i < sizeRows; i++)
            {
                // go over all the elements in some row
                int rowSize = data[i].length - 1;
                for (int j = 0; j < rowSize ; j++)
                {
                    s += data[i][j] + deviderRow;
                }
                s += data[i][rowSize] + deviderLine;
            }
            s = s.substring(0, s.length() - deviderLine.length());
        }
        s += "]";
        return s;
    }

    public static String toString2DimArray(Object[][] data, String deviderRow, String deviderLine)
    {
        String[][] dataStr = new String[data.length][];
        int rowSize = dataStr.length;
        // go over all the rows
        for (int i = 0; i < rowSize; i++)
        {
            dataStr[i] = new String[data[i].length];
            // go over all the elements in some row
            int size = dataStr[i].length;
            for (int j = 0; j < size; j++)
            {
                dataStr[i][j] = data[i][j].toString();
            }
        }
        return toString2DimArray(dataStr, deviderRow, deviderLine);
    }

    public static String toString3DimArray(Object[][][] data, String deviderRow, String deviderLine, String deviderTime)
    {
        logger.addLineToLog("toString list with deviderRow '" + deviderRow +
                "' and with deviderLine = '" + deviderLine + "' and with deviderTime = '" + deviderTime
                + "' is asked over " + data.length*data[0].length*data[0][0].length + " elements");

        String s = "[";
        if(!(data == null || data.length == 0))
        {
            // go over all the elements in the list
            int size = data.length - 1;
            for (int i = 0; i < size; i++)
            {
                s += toString2DimArray(data[i], deviderRow, deviderLine) + deviderTime;
            }
            s += toString2DimArray(data[size], deviderRow, deviderLine);
        }
        s += "]";
        return s;
    }
}
