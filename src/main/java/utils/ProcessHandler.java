package utils;

import java.io.IOException;

/*
    Manage the process start and kill of process from Java and Python
 */
public class ProcessHandler
{
    // members //

    // end - members //

    // const //

    // end - const //

    // shadow constractor
    private ProcessHandler()
    {

    }

    // start a python process and waits until it finishes
    // return the exit code of the process so we be able to understand what happened
    // throws IO if python file does not found, Interrupted if the process clashes in the middle
    public static int runPython(String pythonFilePath) throws IOException, InterruptedException
    {
        String command = "cmd /c python /wait " + pythonFilePath;
        Process p = Runtime.getRuntime().exec(command);
        int exitVal = p.waitFor();
        p.destroy();
        return exitVal;
    }

}
