package utils.file_handler;

import utils.Logger.LoggerConsole;

import java.io.File;
import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

/*
    Generic file handler to manage the functions we do over IO in file system
 */
public class FileHandler
{
    // consts //
    public static LoggerConsole logger = new LoggerConsole();
    public static int FILE_COUNT_ERROR = -1;
    // end - consts //

    // members //

    // end - members //

    // shadow constractor
    private FileHandler()
    {

    }

    // logic functions //

    /**
     * Count how many files in a folder
     * @param folder - folder to check
     * @return - how many files
     */
    public static int countFilesInFolder(String folder)
    {
        Path dir = Paths.get(folder);
        int counter = 0;

        try (DirectoryStream<Path> stream = Files.newDirectoryStream(dir))
        {
            for (Path p : stream)
            {
                counter++;
            }
            return counter;
        }
        catch (IOException ex)
        {
            logger.addLineToLog("Error at countFilesInFolder in  FileHandler with: " + ex.getMessage());
            return FILE_COUNT_ERROR;
        }
    }

    /**
     * delete file from some path
     * @param path - path to file wanted to delete
     * @return if deleted or not
     */
    public static boolean deleteFile(String path)
    {
        // look at link
        File file = new File(path);
        // if we have this file to delete
        if (file.exists())
        {
            file.delete(); // delete file
            logger.addLineToLog("delete file from path = " + path);
            return true; // tell that deleted
        }
        return false; // tell not deleted
    }

    /**
     * check if this folder / file exists
     * @param path - path to file wanted to delete
     * @return - true if found, false if not
     */
    public static boolean exists(String path)
    {
        // look at link
        File file = new File(path);
        // if we have this file
        if (file.exists())
        {
            return true;
        }
        return false;
    }

    /**
     * Create folder in path
     * @param path - path of the folder
     * @return - if we can done it or not
     */
    public static boolean createFolder(String path)
    {
        try
        {
            return new File(path).mkdirs();
        }
        catch (Exception error)
        {
            logger.addLineToLog("Warning at createFolder from FileHandler - did not create folder " + path + " because: " + error.getMessage());
            return false;
        }
    }

    // end - logic functions //
}
