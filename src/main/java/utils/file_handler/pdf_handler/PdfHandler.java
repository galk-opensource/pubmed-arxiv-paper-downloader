package utils.file_handler.pdf_handler;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Arrays;
import java.util.List;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.text.PDFTextStripper;
import org.apache.pdfbox.text.PDFTextStripperByArea;

import org.apache.tika.parser.pdf.PDFParser;
import org.apache.tika.metadata.Metadata;
import org.apache.tika.parser.ParseContext;
import org.apache.tika.sax.BodyContentHandler;
import org.xml.sax.ContentHandler;

import utils.Logger.LoggerJava;
import utils.file_handler.FileHandler;

/*
This class manage the activity we want with a PDF file
 */
public class PdfHandler
{
    // Logger //
    public static LoggerJava logger = new LoggerJava(PdfHandler.class.getName());
    // end - Logger //

    // consts //
    private static String END_PDF_FILE = ".pdf";
    private static int NOISE_SENTANCE_WORD_LENGTH = 2;
    private static int SINGLE_WORD_IN_SENTANCE_LENGTH = 1;
    private static String SPACE = " ";
    private static String DOUBLE_SPACE = "  ";
    private static String ENTER = "\n";
    private static String ERROR_LINE = "~";
    // end - consts //

    ///////////////////
    // academic pdfs //
    ///////////////////

    // input: pdf file path input
    // output: return pdf text
    // action: run all the PDF academic papers and return the order per page with fixed design
    //         and read each one of them and append to the final answer
    public static String getAcademicClearPdf(String pdfPath) throws Exception
    {
        try
        {
            // give the system to fix the pdfs and return image list of fixed pages
            List<BufferedImage> pageImages = PdfToTxt.handle_problems_in_academic_papers(pdfPath);
            // use OCR engine to convert each page into text
            StringBuilder stringBuilder = new StringBuilder();
            for (Image pageImage: pageImages)
            {
                // run ocr and get results
                stringBuilder.append(OCR.read(pageImage));
            }
            return stringBuilder.toString();
        }
        catch (Exception error)
        {
            throw new Exception("Error at getFixedPdfText from PdfHandler with sub error: " + error.getMessage());
        }
    }

    /////////////////////////
    // end - academic pdfs //
    /////////////////////////

    /////////////////////
    // pdfs convectors //
    /////////////////////

    // input: pdf file path input
    // output: return pdf text
    // action: convert pdf to text and clear problematic chars
    // throw exception if file can not been found
    public static String getPdfTextClear(String pdfPath)
    {
        try
        {
            String pdfText = getPdfText(pdfPath);
            pdfText = fixOcrError(pdfText);
            return pdfText;
        }
        catch (Exception error)
        {
            return null;
        }
    }

    // input: pdf file path input
    // output: return pdf text
    // action: just convert pdf to text
    // throw exception if file can not been found
    public static String getPdfText(String pdfPath)
    {
        if(pdfPath.endsWith(END_PDF_FILE))
        {
            try (PDDocument document = PDDocument.load(new File(pdfPath)))
            {
                document.getClass();
                if (!document.isEncrypted())
                {
                    PDFTextStripperByArea stripper = new PDFTextStripperByArea();
                    stripper.setSortByPosition(true);

                    PDFTextStripper tStripper = new PDFTextStripper();

                    return tStripper.getText(document);
                }
            }
            catch (Exception ex)
            {
                logger.addLineToLog("Error with getPdfText in PdfHandler with error: " + ex.getMessage());
                return null;
            }
        }
        logger.addLineToLog("Not Pdf file or document is encrypted");
        throw null;
    }

    // input: pdf file path input
    // output: return pdf text
    // action: just convert pdf to text using Tika library
    // throw exception if file can not been found
    public static void getPdfTextUsingTika(String pdfPath) throws Exception {

        InputStream is = null;
        try
        {
            is = new FileInputStream(pdfPath);
            ContentHandler contenthandler = new BodyContentHandler();
            Metadata metadata = new Metadata();
            PDFParser pdfparser = new PDFParser();
            pdfparser.parse(is, contenthandler, metadata, new ParseContext());
            System.out.println(contenthandler.toString());
        }
        catch (Exception error)
        {
            throw new Exception("Error at getPdfTextUsingTika from PdfHandler - " + error.getMessage());
        }
        finally
        {
            if (is != null)
            {
                is.close();
            }
        }
    }

    ///////////////////////////
    // end - pdfs convectors //
    ///////////////////////////

    ////////////////////
    // help functions //
    ////////////////////

    /**
     * delete pdf file from some path
     * @param pdfPath - path to file wanted to delete
     * @return if deleted or not
     */
    public static boolean deleteFile(String pdfPath)
    {
        // if pdf file
        if(pdfPath.endsWith(END_PDF_FILE))
        {
            return FileHandler.deleteFile(pdfPath);
        }
        return false; // tell not deleted
    }


    /**
     * read each file from the results, clear errors of the OCR and save again
     * @param pdfText - the original text
     * @return - the same text without errors
     */
    private static String fixOcrError(String pdfText)
    {
        // remove from all the document unwanted chars
        pdfText = pdfText.replaceAll("[^a-zA-Z0-9 :=.,;!\n\r?+\\-\\]\\[()]", "");

        // split texts to lines (not logical senances)
        List<String> lines = Arrays.asList(pdfText.split(ENTER)); // split according to lines
        // run over each line
        for (int lineIndex = 0; lineIndex < lines.size(); ++lineIndex)
        {
            // fix spliting chars before split
            lines.set(lineIndex, lines.get(lineIndex).trim());
            while(lines.get(lineIndex).contains(DOUBLE_SPACE))
            {
                lines.set(lineIndex, lines.get(lineIndex).replace(DOUBLE_SPACE, SPACE));
            }
            // split to words
            String[] words = lines.get(lineIndex).split(SPACE);
            // analysis each line //
            // if line with less then some amount of words then it's noise -> delete it
            // if only numbers in array then it error as well
            //TODO: change to smart word counting when math symbols and common math letters ignored
            if (smartSentenceWordLength(words) == SINGLE_WORD_IN_SENTANCE_LENGTH && isNumeric(words[0]))
            {
                lines.set(lineIndex, ERROR_LINE);
            }
            else if (smartSentenceWordLength(words) <= NOISE_SENTANCE_WORD_LENGTH || isAllNumeric(words))
            {
                lines.set(lineIndex, ERROR_LINE);
            }
        }

        boolean pass_reference = false;
        for (int lineIndex = 0; lineIndex < lines.size(); ++lineIndex)
        {
            // if we in the reference section - just remove lines
            if (pass_reference)
            {
                lines.remove(lineIndex);
                continue;
            }
            // check if we see reference line
            if (lines.get(lineIndex).toLowerCase().startsWith("reference") && lines.get(lineIndex).split(" ").length == SINGLE_WORD_IN_SENTANCE_LENGTH)
            {
                // from this point, we are in referances
                pass_reference = true;
            }
        }

        // build answer
        StringBuilder answerBuilder = new StringBuilder();
        for (String line: lines)
        {
            // make sure this is not error line we deleted earlier
            if (!line.equals(ERROR_LINE))
            {
                answerBuilder.append(line).append("\n");
            }
        }
        return answerBuilder.toString();
    }

    /**
     * calc the length of a sentence in a smart way
     * @param words - the sentence as words to count
     * @return - how many passed words
     */
    private static int smartSentenceWordLength(String[] words)
    {
        int answer = 0;
        // run over each word
        for (String word: words)
        {
            // check if a word we want to count
            if(word.replaceAll("[^a-zA-Z]", "").length() > NOISE_SENTANCE_WORD_LENGTH)
            {
                answer++;
            }
        }
        return answer;
    }

    /**
     * check if all the words are numbers or not
     * @param words - array of words (strings)
     * @return - bool
     */
    private static boolean isAllNumeric(String[] words)
    {
        for (String word: words)
        {
            if (!isNumeric(word))
            {
                return false;
            }
        }
        return true;
    }

    /**
     * check if a word is a number
     * @param str - the word to check
     * @return - bool
     */
    private static boolean isNumeric(String str) {
        return str.matches("-?\\d+(\\.\\d+)?");  //match a number with optional '-' and decimal.
    }

    //////////////////////////
    // end - help functions //
    //////////////////////////
}
