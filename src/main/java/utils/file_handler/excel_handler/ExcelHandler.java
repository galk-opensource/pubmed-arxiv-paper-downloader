package utils.file_handler.excel_handler;

import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.FileOutputStream;
import java.io.IOException;

/*
    Handle excel read and write files
 */
public class ExcelHandler
{
    // members //
    private String[] columns;
    // end - members //

    // consts //
    public static boolean IS_FONT_BOLD = false;
    public static int FONT_SIZE = 16;
    // end - consts //

    // constractor
    public ExcelHandler(String[] columns)
    {
        if (columns != null && columns.length > 0)
        {
            this.columns = columns;
        }
        else
        {
            this.columns = null;
        }
    }

    /**
     * write according to some property list the data from
     * @param sheetName - the name of the sheets
     * @param data - the data we wish to write
     * @throws IOException
     */
    public void write(String sheetName, String[][] data, String filePath) throws IOException
    {
        // Create a Workbook
        Workbook workbook = new XSSFWorkbook(); // new HSSFWorkbook() for generating `.xls` file

        /* CreationHelper helps us create instances of various things like DataFormat,
           Hyperlink, RichTextString etc, in a format (HSSF, XSSF) independent way */
        CreationHelper createHelper = workbook.getCreationHelper();

        // Create a Sheet
        Sheet sheet = workbook.createSheet(sheetName);

        // Create a Font for styling header cells
        Font headerFont = workbook.createFont();
        headerFont.setBold(IS_FONT_BOLD);
        headerFont.setFontHeightInPoints((short) FONT_SIZE);
        headerFont.setColor(IndexedColors.BLACK.getIndex());

        // Create a CellStyle with the font
        CellStyle headerCellStyle = workbook.createCellStyle();
        headerCellStyle.setFont(headerFont);

        // Create a Row
        Row headerRow = sheet.createRow(0);

        // Create cells
        for(int i = 0; i < this.columns.length; i++)
        {
            Cell cell = headerRow.createCell(i);
            cell.setCellValue(columns[i]);
            cell.setCellStyle(headerCellStyle);
        }

        // Create Other rows and cells with employees data
        int rowNum = 1;
        for(String[] rowData: data)
        {
            // create row
            Row row = sheet.createRow(rowNum++);
            int itemNum = 0;
            for (String item: rowData)
            {
                row.createCell(itemNum++).setCellValue(item);
            }
        }

        // Resize all columns to fit the content size
        for(int i = 0; i < this.columns.length; i++) {
            sheet.autoSizeColumn(i);
        }

        // Write the output to a file
        FileOutputStream fileOut = new FileOutputStream(filePath);
        workbook.write(fileOut);
        fileOut.close();

        // Closing the workbook
        workbook.close();
    }
}