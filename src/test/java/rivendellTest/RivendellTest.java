package rivendellTest;

import libraryProcess.LibraryManager;
import org.junit.Test;
import rivendell.AbstractRivendell;
import rivendell.RivendellApi;
import utils.Logger.LoggerConsole;
import utils.file_handler.FileHandler;
import utils.file_handler.txt_handler.TxtHandler;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.*;

/*
    This class manage the global library process of testing
 */
public class RivendellTest
{
    public static LoggerConsole logger = new LoggerConsole();

    // consts //
    private List<String> queries =  Arrays.asList("nanoparticle");
    private String RivendellKey = "3CfntafaGEXHm9oKWzQ/q+F/O/JZe1vNFqo3s6Nh63HoQJ6peOdiMS/ukwwbKRQiCp0yNeG47iTVTo6xOm/g2g==";
    // end - consts //

    @Test
    public void testRivedellQuery()
    {
        try
        {
            int topNListSize = 30;

            RivendellApi libraryManager = new RivendellApi(RivendellKey);
            for (String query: queries)
            {
                List<AbstractRivendell> queryAnswers = libraryManager.runQuery(query);
                Collections.shuffle(queryAnswers);
                String[] queryAnswersTopN = new String[topNListSize];
                for (int index = 0; index < topNListSize; index++)
                {
                    queryAnswersTopN[index] = queryAnswers.get(index).getTitle();
                    logger.addLineToLog(queryAnswers.get(index).getTitle().replace("\n", " "));
                }
                if (queryAnswers.size() != 0)
                {
                    throw new Exception("Query '" + query + "' does no return any answer, test failed");
                }
            }
            logger.addLineToLog("Test with  " + queries.size() + " queries passed");
        }
        catch (Exception ex)
        {
            logger.addLineToLog("Error at testRivedellQuery in SystemTest with: " + ex.getMessage());
        }
    }
}
