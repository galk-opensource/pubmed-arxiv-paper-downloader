# Pubmed, Arxiv Paper Downloader

The repo contains code allowing a user to download full papers from both PubMed and arXiv.  Currently, it support the following operations:

1. Download all papers answering some query (using on of the service's search engine or using [Rivendell](http://rivendell.cs.biu.ac.il), the academic search engine.
2. Download a specific paper from PubMed using either its title or PMID. 
3. Download a specific paper from arXiv using its title.

## Background

This repo is a part of a bigger project asking to extract instances of medical nanorobotics from academic papers.

## Usage 

Install: No installation as this is a service in a bigger process.
One can use the library at any way needed.


## Performance
 
Tested on 1000 random papers from both arXiv and PubMed, the results find to be: 65%, 48% success rate for arXiv and PubMed, respectively.

## Example
 
Example for both the cases:

1. Downloading using the paper's title:

```java
Iselenium iselenium = new Iselenium();
String[] links = {"http://iopscience.iop.org/article/10.1088/1361-6528/aace99", "https://www.tandfonline.com/doi/abs/10.1080/17425255.2018.1491965", "https://link.springer.com/article/10.1007%2Fs13659-018-0170-1"};
List<String> answers = new ArrayList<>();
for (int i = 0; i < links.length; i++)
{
	answers.addAll(pdfDownloader.getPdfLink(iselenium.getHtmlFromUrl(links[i])));
}

if (answers.get(0).equals("/article/10.1088/1361-6528/aace99/pdf") &&
		answers.get(1).equals("https://www.tandfonline.com/doi/abs/10.1080/17425255.2018.1491965?needAccess=true#aHR0cHM6Ly93d3cudGFuZGZvbmxpbmUuY29tL2RvaS9wZGYvMTAuMTA4MC8xNzQyNTI1NS4yMDE4LjE0OTE5NjU/bmVlZEFjY2Vzcz10cnVlQEBAMA==") &&
		answers.get(2).equals("/content/pdf/10.1007%2Fs13659-018-0170-1.pdf"))
{
	final String folderPath = "<your_path>";
	pdfDownloader.saveToFile(new URL(UrlUtil.getAbsultPath(links[0], answers.get(0))), new File(folderPath + "\\test0.pdf"));
	pdfDownloader.saveToFile(new URL(UrlUtil.getAbsultPath(links[1], answers.get(1))), new File(folderPath + "\\test1.pdf"));
	pdfDownloader.saveToFile(new URL(UrlUtil.getAbsultPath(links[2], answers.get(2))), new File(folderPath + "\\test2.pdf"));
}
```

2. Downloading using a query in Rivendell (inner search engine is just using the settings of the [API](http://rivendell.cs.biu.ac.il/Home/ApiInfo) :

```java
RivendellApi rivendellApi = new RivendellApi(RivendellKey);
for (String query: queries)
{
	List<AbstractRivendell> queryAnswers = rivendellApi.runQuery(query); // change here to get the results from a spesific source
	String[] queryAnswersArray = new String[queryAnswers.size()];
	for (int index = 0; index < queryAnswersArray.length; index++)
	{
		queryAnswersArray[index] = queryAnswers.get(index).getTitle();
	}
	if (queryAnswers.size() != 0)
	{
		throw new Exception("Query '" + query + "' does no return any answer, test failed");
	}
}
```
